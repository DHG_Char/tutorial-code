#ifndef DHG_POINT3D
#define DHG_POINT3D

class cPoint3D
{
public:
    // Constructors / Destructors
    cPoint3D()
    ~cPoint3D()
    
    // Return a unit direction vector for the input data.
    cPoint Unit();
    
    // Return the magnitude if this point were used for a direction vector
    float Magnitude();
    
    // Math Operations on points
    // Piecewise Subtraction
    cPoint3D operator-(cPoint3D p2);
    // Piecewise Addition
    cPoint3D operator+(cPoint3D p2);
    // Piecewise Multiplication
    cPoint3D operator*(float fValue);
    // Piecewise Division
    cPoint3D operator/(float fValue);
    
public:
    // Public Data Elements
    float fX;
    float fY;
    float fZ;
};
#endif // DHG_POINT3D