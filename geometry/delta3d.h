#ifndef DHG_DELTA3D
#define DHG_DELTA3D

#include <point3d>

// A Delta is a simple vector.  Defined as P0 = Origin.
// See cVector3D for a complex vector with an arbitrary P0
class cDelta3D
{
public:
    // Constructors / Destructors
    cDelta3D();
    cDelta3D(cPoint3D p0, cPoint3D p1);
    cDelta3D(float fMag, cPoint3D dir);
    ~cDelta3D();
    
    // Math Operations on deltas
    // Cross Product
    cDelta3D operator/(cDelta3D d2);
    // Dot Product
    cDelta3D operator*(cDelta3D d2);
    // Location along the delta
    cPoint3D operator*(float fSteps);
    
public:
    // Public Data Elements
    float fMagnitude;
    float fX;
    float fY;
    float fZ;
};
#endif // DHG_DELTA3D