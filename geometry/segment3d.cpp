// #include <math> // You will need to include your math library to get 
#include <segment3d>

// Basic Constructor 
cSegment3D::cSegment3D()
{}

// Constructor for segment between two points
cSegment3D::cSegment3D(cPoint3D pStart, cPoint3D pEnd)
{
    p1 = pStart;
    p2 = pEnd;
    delta = cDelta3D(pStart, pEnd);
}

// Constructor for a vector
cSegment3D::cSegment3D(cVector3D v, float fSteps)
{
    p1 = v.p0;
    p2 = v.p0 + (v.delta * fSteps);
    delta = cDelta3D(p1, p2);
}

// Destructor
cSegment3D::~cSegment3D()
{}

// Line Interaction functions
float cSegment3D::MaxDistance(cSegment3D s2)
{}