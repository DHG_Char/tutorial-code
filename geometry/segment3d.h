#ifndef DHG_SEGMENT3D
#define DHG_SEGMENT3D

#include <vector3d>
#include <line3d>

class cSegment3D : public cLine3D
{
public:
    // Constructors and Destructors
    cSegment3D();
    cSegment3D(cPoint3D pStart, cPoint3D pEnd);
    cSegment3D(cVector3D v, float fSteps);
    ~cSegment3D();

    // Line Interaction functions
    float MaxDistance(cSegment3D s2);
    
public:    
    // Data for the Segment
    cPoint3D p1;
    cPoint3D p2;
    cDelta3D delta;
};

#endif // DHG_SEGMENT3D