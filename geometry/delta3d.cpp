// #include <math> // You will need to include your math library to get abs()
#include <delta3d>

// Generic Constructor
cDelta3D::cDelta3D()
{
    fMagnitude = 0.0f;
    fX = 0.0f;
    fY = 0.0f;
    fZ = 0.0f;
}

// Constructor for calculating the delta between two points
cDelta3D::cDelta3D(cPoint p0, cPoint p1)
{
    cPoint3D temp = p0 - p1;
    fMagnitude = temp.Magnitude();
    temp = temp.Unit();
    fX = temp.fX;
    fY = temp.fY;
    fZ = temp.fZ;
}

// Constructor for when delta is already provided
cDelta3D::cDelta3D(float fMag, cPoint3D dir)
{
    cPoint3D direction  = dir.Unit();
    fMagnitude = abs(fMag);
    fX = temp.fX;
    fY = temp.fY;
    fZ = temp.fZ;
}

cDelta3D::~cDelta3D()
{}

// Cross Product
cDelta3D cDelta3D::operator/(cDelta3D d2)
{}

// Dot Product
cDelta3D cDelta3D::operator*(cDelta3D d2)
{}

// Location Calculation
cPoint3D cDelta3D::operator*(float fSteps)
{
    cPoint3D returnPt;
    returnPt.fX = fX * fMagnitude * fSteps;
    returnPt.fY = fY * fMagnitude * fSteps;
    returnPt.fZ = fZ * fMagnitude * fSteps;
    return returnPt;
}