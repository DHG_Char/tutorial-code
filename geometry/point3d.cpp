// #include <math> // You'll need to include your math library to get sqrtf()
#include <point3d>

// Constructor
cPoint3D::cPoint3D()
{
    fX = 0.0f;
    fY = 0.0f;
    fZ = 0.0f;
}

// Destructor
cPoint3D::~cPoint3D()
{}

// Return the magnitude if this point were used for a direction vector
float cPoint3D::Magnitude()
{
    float fLength = 0.0f;
    fLength = sqrt(fX * fX + fY * fY + fZ * fZ);
    return fLength;
}

// Return a unit direction vector for the input data.
cPoint cPoint3D::Unit()
{
    float fLength = 0.0f;
    cPoint returnPt;
    fLength = Magnitude();
    if(fLength != 0.0f)
    {
        returnPt.fX = fX / fLength;
        returnPt.fY = fY / fLength;
        returnPt.fZ = fZ / fLength;
    }
    return returnPt;
}

// Piecewise Subtraction
cPoint3D cPoint3D::operator-(cPoint3D p2)
{
    cPoint3D out;
    out.fX = fX - p2.fX;
    out.fY = fY - p2.fY;
    out.fZ = fZ - p2.fZ;
    return out;
}

// Piecewise Addition
cPoint3D cPoint3D::operator+(cPoint3D p2)
{
    cPoint3D out;
    out.fX = fX + p2.fX;
    out.fY = fY + p2.fY;
    out.fZ = fZ + p2.fZ;
    return out;
}

// Piecewise Multiplication
cPoint3D cPoint3D::operator*(float fValue)
{
    cPoint3D out;
    out.fX = fX * fValue;
    out.fY = fY * fValue;
    out.fZ = fZ * fValue;
    return out;
}

// Piecewise Division
cPoint3D cPoint3D::operator/(float fValue)
{
    cPoint3D out;
    if(fValue != 0.0f)
    {
        out.fX = fX / fValue;
        out.fY = fY / fValue;
        out.fZ = fZ / fValue;
    }
    return out;
}
